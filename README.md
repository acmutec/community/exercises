# Repository of solved exercises for CS courses

In this repository, we'll aim to provide as much solved exercises as we can. 

Be aware that some of the solutions might not be completely correct. 

If you want to submit a correction, then please pull request a commit with the appropriate modification.

---

Collaborators:
- Diego Cánez (@dgcnz)
- César Salcedo (@csalcedo001)